Projet Android 
==============

*Auteurs : DARRIGOL Marie, LEONARD Anthony, LIBRALESSO Luc, PELLOUX-PRAYER Ophélie*

Langages utilisés : Android, Python, Java

1) App_Android
------------------

Le dossier App_Android contient le code source de l'application android.

Pour le moment la maquette de l'application est la suivante :

.. image:: https://bytebucket.org/ricm4_android/projet-android/raw/38ef506140430028668e27a06afd33c4be27b1af/doc_usr/images/maquette.jpg?token=120d6898758063d98e66dbfb3a5ec85ed4543d85
   :height: 520px
   :width: 697px
   :scale: 70 %
   :alt: maquette
   :align: right

2) Serveur
----------

Le dossier Serveur contient le code source du serveur google app engine.

.. image:: https://bytebucket.org/ricm4_android/projet-android/raw/38ef506140430028668e27a06afd33c4be27b1af/doc_usr/images/diagrammeG%C3%A9n%C3%A9ral.png?token=b7a5750b0805a94cd702d20dab78b12be62ad329
   :height: 710px
   :width: 1418px
   :scale: 50 %
   :alt: diagrammeGénéral
   :align: right
   

Le serveur fonctionnel sur le cloud de google est disponible ici : http://ricm4indoor.appspot.com/
Un fichier json de test est disponible ici : http://ricm4indoor.appspot.com/files/test.json



TODOLIST
========

 - Ajout de la synchronisation automatique (sans avoir a appuyer sur refresh)
 - Stockage du fichier json dans un(e) variable/fichier sur le device android
     - au cas où une simple variable n'est pas suffisante (problème éventuel de place mémoire),
       on peut le stocker dans un fichier
 - Réalisation de fichiers html pour les tests
 - Réalisation de fichier html dans le cas d'une erreur (si la puce n'a pas été trouvé dans le fichier json)
 - implémentation de la partie bluetooth pour récupérer l'id (ou adresse MAC) de la puce
 - parsing du fichier json et on retourne le html et le lieu correspondants à l'id demandé en paramètre
 - préparation soutenance :
     - intérêt et fonctionnalités de l'application
     - démonstration application
     - présentation app engine
     - présentation bluetooth
     - fonctionnalités futures

