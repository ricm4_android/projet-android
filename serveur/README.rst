
Pour une utilisation locale
===========================

Il est nécessaire d'avoir installé python2.7 (installé de base sur la
plupart des distributions linux)

Il faut également avoir téléchargé google app engine pour python :
https://storage.googleapis.com/appengine-sdks/featured/google_appengine_1.9.15.zip

Dézipper le fichier téléchargé : 

.. parsed-literal::
	google_appengine_1.9.15.zip
	cd google_appengine/
	
puis lancer la commande suivante :

.. parsed-literal::
	python2.7 dev_appserver.py <répertoire de l'application>

Le serveur étant lancé, on peut accèder à l'application via http://localhost:8080/
ou http://localhost:8000/.
	
