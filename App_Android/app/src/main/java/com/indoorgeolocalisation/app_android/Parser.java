package com.indoorgeolocalisation.app_android;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Ophelie on 12/12/2014.
 */
public class Parser {

    public static void parser(String fichier,  DBOpenHelper db) throws JSONException {
        JSONObject jsonObject = new JSONObject(fichier);

        // Get the first array of elements
        JSONArray puces = jsonObject.getJSONArray("puce");

        for (int i = 0; i < puces.length(); i++) {

            JSONObject puce = puces.getJSONObject(i);

            String id = puce.getString("id");
            String name = puce.getString("name");
            String html = puce.getString("html");

            db.ecrireValeur(id,name, html);
        }
    }

}