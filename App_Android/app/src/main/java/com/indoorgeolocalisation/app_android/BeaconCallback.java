package com.indoorgeolocalisation.app_android;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thony on 12/12/14.
 */
public class BeaconCallback implements BluetoothAdapter.LeScanCallback{
    private String mac;
    private int sigforce;

    public BeaconCallback() {
        sigforce = -65536;
    }

    @Override
    public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bytes) {
        Log.v("Scan", bluetoothDevice.getAddress() + "Signal : " + i);
        if(i > sigforce) { // On cherche à conserver la balise la plus proche.
            mac = bluetoothDevice.getAddress();
            sigforce = i;
        }
    }

    public void clear(){
        mac = null;
        sigforce = -65536;
    }

    public String getMac() {
        return mac;
    }

}
