package com.indoorgeolocalisation.app_android;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.AsyncTask;
import android.webkit.WebView;
import android.widget.TextView;

/**
 * Created by thony on 05/01/15.
 */
public class BtScanner extends AsyncTask<Void, Void, String> {
    private BluetoothAdapter ba;
    private BeaconCallback bc;
    private DBOpenHelper db;
    private TextView tv;
    private WebView wv;
    private InfoFragment IF;

    public BtScanner(Context ctx, TextView tv, WebView wv, InfoFragment IF){
        super();
        ba = BluetoothAdapter.getDefaultAdapter();
        bc = new BeaconCallback();
        db = new DBOpenHelper(ctx);
        this.tv = tv;
        this.wv = wv;
        this.IF = IF;
    }

    @Override
    protected String doInBackground(Void... voids) {
        String mac;
        ba.startLeScan(bc);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ba.stopLeScan(bc);
        mac = bc.getMac();
        bc.clear();
        return mac;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Couple couple;
        if(s != null) {
            couple = db.lireValeur(s);
            if(couple != null) {
                IF.webData = couple.html();
                wv.loadData(IF.webData, "text/html", null);
                tv.setText(couple.name());
            }
        }
    }
}
