package com.indoorgeolocalisation.app_android;

/**
 * Created by Ophelie on 12/12/2014.
 */
public class Couple {
    String name;
    String html;

    public Couple(String name, String html){
        this.name = name;
        this.html = html;
    }

    public String name(){
        return name;
    }
    public String html(){
        return html;
    }

    public String toString(){
        return "Couple [ name : " + name + "html : " + html + "]";
    }
}
