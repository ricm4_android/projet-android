package com.indoorgeolocalisation.app_android;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONException;

import java.util.HashMap;


public class POI_List extends Activity {

    Handler handler;
    DBOpenHelper db;
    private Runnable runnable = new Runnable(){

        @Override
        public void run() {
            InfoFragment fragment = (InfoFragment)getFragmentManager().findFragmentByTag("FRAGMENT");
            if (fragment != null ) {
                fragment.executeSync();
            }
            handler.postDelayed(runnable, 180000);
        }
    };

    /**
     * Met à jour la base de données en lisant les données reçues dans rawData
     * @param rawData
     */
    public void updateDB(String rawData){
        //Log.v("DATA", rawData);
        try {
            Parser.parser(rawData, db);
        } catch (JSONException e) {
            e.printStackTrace();
        }
       //Log.v("DATA IN DB", "1: " + db.lireValeur("1"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DBOpenHelper(getApplicationContext());
        setContentView(R.layout.activity_poi__list);
        if (savedInstanceState == null){
            InfoFragment fragment = new InfoFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, fragment, "FRAGMENT")
                    .commit();
        }
        handler = new Handler();
        handler.postDelayed(runnable, 1000);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.poi__list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public DBOpenHelper db(){
        return db;
    }
}
