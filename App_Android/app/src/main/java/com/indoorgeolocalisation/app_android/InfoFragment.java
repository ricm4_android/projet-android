package com.indoorgeolocalisation.app_android;

import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class InfoFragment extends Fragment {

    public WebView myWebView;
    public TextView myTextView;
    public String webData;
    public InfoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu (Menu menu, MenuInflater inflater){
        inflater.inflate(R.menu.poi__list,menu);
    }

    public void executeSync(){
        FetchInfoTask infoTask = new FetchInfoTask();
        infoTask.execute();
    }

    public void executeScan() {
        BtScanner btScan = new BtScanner(getActivity().getApplicationContext(), myTextView, myWebView,this);
        btScan.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if ( id == R.id.action_refresh ) {
            executeSync();
            return true;
        }
        if (id == R.id.action_scan ) {
            BluetoothAdapter ba = BluetoothAdapter.getDefaultAdapter();
            if (ba == null || !ba.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            } else {
                executeScan();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateAffichage(String mac) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.my_fragment, container, false);

        myWebView = (WebView) rootView.findViewById(R.id.affichage_virtuel);
        myTextView = (TextView) rootView.findViewById(R.id.interet_courant);
        String textData = "Accueil";
        webData = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n" +
                "<html>\n" +
                "\t<head>\n" +
                "\t\t<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n" +
                "\t\t<title>Indoor Geolocalisation</title>\n" +
                "\t</head>\n" +
                "\t<body style=\"margin: 0px; padding: 0px; font-size: 12px;\">\n" +
                "\t\t<h2 style=\"box-shadow: 2px 2px 2px rgb(48, 48, 48); font-family: Georgia; text-align: center; color: white; background-color: rgb(67, 78, 222); margin: 5px; padding: 20px 10px; position: absolute; right: 0px; left: 0px; height: 20pt; font-size: 1.5em;\">\n" +
                "\t\t\tBienvenue dans Indoor Geolocalisation\n" +
                "\t\t</h2>\n" +
                "\t\t<div style=\"box-shadow: 2px 2px 2px rgb(48, 48, 48); background-color: rgb(110, 118, 231); padding: 20px 10px; font-family: Georgia; text-align: justify; color: white; position: absolute; margin: 5px; top: 53pt; bottom: 55pt; font-size: 1.2em;\">\n" +
                "\t\t\t<p style=\"\">\n" +
                "\t\t\t\tCette application permet de r&eacute;cup&eacute;rer des informations sur le b&acirc;timent dans lequel vous vous trouvez.\n" +
                "                Il vous suffit de vous trouver pr&egrave;s d'une des puces Bluetooth install&eacute;es dans le b&acirc;timent et d'activer le scan.\n" +
                "                Vous recevrez alors toutes les informations sur votre appareil, tablette ou mobile.\n" +
                "\t\t\t\t<br>\n" +
                "\t\t\t\tCe projet a &eacute;t&eacute; r&eacute;alis&eacute; dans le cadre de notre cours d'Android de 4&egrave;me ann&eacute;e d'&eacute;cole d'ing&eacute;nieur.\n" +
                "\t\t\t\t<br>\n" +
                "\t\t\t</p>\n" +
                "\t\t\t<img style=\"display : block; position : relative; margin : 0 auto;\" src=\"ic_launcher.png\">\n" +
                "\t\t</div>\n" +
                "\t\t<div id=\"separator\" style=\"clear:both\"></div>\n" +
                "\t\t\t<p style=\"box-shadow: 2px 2px 2px rgb(48, 48, 48); background-color: rgb(47, 59, 212); font-family: Georgia; text-align: center; color: white; margin: 5px; padding: 20px 10px; position: absolute; bottom: 0px; left: 0px; right: 0px; min-height: 20pt; font-size: 1em;\">\n" +
                "\t\t\t\tDARRIGOL Marie - LEONARD Anthony - LIBRALESSO Luc - PELLOUX-PRAYER Oph&eacute;lie\n" +
                "\t\t\t\t<br>\n" +
                "\t\t\t\tPolytech' Grenoble - 2014-2015\n" +
                "\t\t\t</p>\n" +
                "\t</body>\n" +
                "</html>";
        myWebView.loadData(webData, "text/html", null);
        myTextView.setText(textData);
        return rootView;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if ( savedInstanceState != null ){
            String webData = savedInstanceState.getString("web","");
            String textData = savedInstanceState.getString("text","");
            myWebView.loadData(webData, "text/html", null);
            myTextView.setText(textData);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("web", webData);
        outState.putString("text",myTextView.getText().toString());
    }


    public class FetchInfoTask extends AsyncTask<String,Void,String> {

        private final String LOG_TAG = FetchInfoTask.class.getSimpleName();

        @Override
        protected String doInBackground(String... params) {
            // These two need to be declared outside the try/catch
            // so that they can be closed in the finally block.
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            // Will contain the raw JSON response as a string.
            String infoJsonStr = null;
            try {
                // using Uri.Builder for maintainable requests
                Uri.Builder uri_info = new Uri.Builder();
                uri_info.scheme("http");
                uri_info.authority("ricm4indoor.appspot.com");
                uri_info.appendEncodedPath("files");
                uri_info.appendEncodedPath("test.json");
                //Log.d("DEBUG:",uri_info.build().toString());
                URL url = new URL(uri_info.build().toString());
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    infoJsonStr = null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }
                if (buffer.length() == 0) {
                    // Stream was empty. No point in parsing.
                    infoJsonStr = null;
                }
                infoJsonStr = buffer.toString();
            } catch (IOException e) {
                Log.e("PlaceholderFragment", "Error ", e);
                // If the code didn't successfully get the weather data, there's no point in attempting
                // to parse it.
                infoJsonStr = null;
            } finally{
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e("PlaceholderFragment", "Error closing stream", e);
                    }
                }
            }
            if ( infoJsonStr == null )
                return null;
            return infoJsonStr;
        }

        public void onPostExecute(String res) {
            Context context = getActivity().getApplicationContext();
            int duration = Toast.LENGTH_SHORT;
            CharSequence text;
            if (res == null ){
                text = "Problème de connexion, les données n'ont pas pu être téléchargées.";
            } else {
                text = "Les données ont pu être téléchargées.";
                ((POI_List) getActivity()).updateDB(res);
            }
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }

    }

}
