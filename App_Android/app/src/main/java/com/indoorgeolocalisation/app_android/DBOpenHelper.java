package com.indoorgeolocalisation.app_android;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by thony on 12/12/14.
 */
public class DBOpenHelper extends SQLiteOpenHelper {
    private static final String DB_TABLE = "indoorgeoloc";
    private static final String DB_CREATE = "CREATE TABLE "+DB_TABLE+" ( " +
            "mac TEXT PRIMARY KEY," +
            "nom  TEXT NOT NULL," +
            "html TEXT NOT NULL" +
            ");";
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DB_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        deleteTable(db);
        onCreate(db);
    }

    public void deleteTable(SQLiteDatabase db){
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE);
    }

    public void ecrireValeur(String mac, String nom, String html){
        int updRet;
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues content = new ContentValues();
        content.put("mac",mac);
        content.put("nom",nom);
        content.put("html",html);
        // On commence par tenter une mise à jour.
        updRet = db.update(DB_TABLE, content, "mac = ?", new String[]{mac});
        if(updRet == 1) { // Si mise à jour réussie.
            db.setTransactionSuccessful(); // On marque la transaction comme réussie
        } else { // Sinon, ajout d'une nouvelle entrée
            if (db.insert(DB_TABLE, null, content) < 0) {
                Log.e("WRITE_DB", "Erreur d'écriture en BD.");
            } else {
                db.setTransactionSuccessful();
            }
        }
        db.endTransaction();
    }

    public Couple lireValeur(String mac){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("select * from "+DB_TABLE+" WHERE mac = ?", new String[]{mac});
        Couple couple = null;
        if(c != null && c.getCount() > 0){
           c.moveToFirst();
           couple = new Couple(
                    c.getString(c.getColumnIndex("nom")),
                    c.getString(c.getColumnIndex("html"))
           );
        }
        c.close();
        return couple;
    }


    public DBOpenHelper(Context ctx) {
        super(ctx, DB_TABLE, null, 1);
    }
}
