  public void onCreate(Bundle savedInstanceState)  
{
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);

    this.mHandler = new Handler();

    this.mHandler.postDelayed(m_Runnable,5000);


}//onCreate

private final Runnable m_Runnable = new Runnable()
{
    public void run()

    {
        Toast.makeText(refresh.this,"in runnable",Toast.LENGTH_SHORT).show();

        refresh.this.mHandler.postDelayed(m_Runnable, 5000);            
    }

};//runnable




Handler handler = new Handler();
Runnable timedTask = new Runnable(){

    @Override
    public void run() {
        getUrlText();
        handler.postDelayed(timedTask, 1000);
    }};

Then set your onClick:

button.setOnClickListener(new OnClickListener() {

    @Override
        public void onClick(View v) {
            getUrlText();
            handler.post(timedTask);
        }
    });
